<?php

namespace Drupal\form_pack\Service;

use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a common interface for FormPack general services.
 */
interface FormPackGeneralServiceInterface {

  /**
   * Alters a form, checks for form type / context.
   */
  public function formAlter(&$form, FormStateInterface $form_state, $form_id);

  /**
   * Specifically alters a ConfigEntityBundleBase form.
   */
  public function formAlterConfigEntityBundleBase(&$form, FormStateInterface $form_state, $form_id);

}
