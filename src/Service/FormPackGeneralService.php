<?php

namespace Drupal\form_pack\Service;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * General FormPack service.
 */
class FormPackGeneralService implements FormPackGeneralServiceInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function formAlter(&$form, FormStateInterface $form_state, $form_id) {
    $form_object = $form_state->getFormObject();

    if (!method_exists($form_object, 'getEntity')) {
      return;
    }

    if ($form_object->getEntity() instanceof ConfigEntityBundleBase) {
      $this->formAlterConfigEntityBundleBase($form, $form_state, $form_id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function formAlterConfigEntityBundleBase(&$form, FormStateInterface $form_state, $form_id) {
    $form_object = $form_state->getFormObject();
    $entity = $form_object->getEntity();

    if (!isset($form['additional_settings'])) {
      $form['additional_settings'] = [
        '#type' => 'vertical_tabs',
        '#default_tab' => 'form_pack',
      ];
    }

    $values = $entity->getThirdPartySetting('form_pack', 'form_pack', NULL);

    $form['form_pack'] = [
      '#type' => 'details',
      '#title' => $this->t('Form pack'),
      '#group' => 'additional_settings',
      '#weight' => 0,
      '#tree' => TRUE,
    ];

    $form['form_pack']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Form Pack support'),
      '#description' => $this->t('Check this to enable all of the Form Pack features.'),
      '#default_value' => $values['enable'] ?? 0,
    ];
    $form['form_pack']['enable']['#attributes']['class'][] = 'form_pack_enabled';

    // General settings.
    $form['form_pack']['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#states' => [
        'visible' => [
          '.form_pack_enabled' => ['checked' => TRUE],
        ],
      ],
    ];

    // @TODO available as block.
    // Maybe something to do with which form_display to use? What to do
    // with multiple form displays.
    // Revision handling (need to detect if there are revisions).
    // Probably put resubmission stuff here.
    $form_parent = &$form['form_pack']['general'];
    $form_parent_values = $values['general'] ?? [];

    $options = [
      '' => $this->t('- Select -'),
      'message' => $this->t('Show message'),
      'edit' => $this->t('Edit previous submission'),
      'show' => $this->t('Show previous submission'),
      'new' => $this->t('Create new submission'),
    ];

    $form_parent['resubmission'] = [
      '#type' => 'select',
      '#title' => $this->t('Multiple submissions'),
      '#description' => $this->t('How to handle users making multiple submissions.'),
      '#default_value' => $form_parent_values['resubmission'] ?? '',
      '#options' => $options,
    ];

    // @TODO if message, will need to store. Could also be error message.
    // @TODO submission limits.

    // Access settings.
    $form['form_pack']['access'] = [
      '#type' => 'details',
      '#title' => $this->t('Access settings'),
      '#states' => [
        'visible' => [
          '.form_pack_enabled' => ['checked' => TRUE],
        ],
      ],
    ];

    // @TODO select roles that can submit.
    // Field to use for status.
    // Resubmission options.

    // Form settings.
    $form['form_pack']['form_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Form settings'),
      '#states' => [
        'visible' => [
          '.form_pack_enabled' => ['checked' => TRUE],
        ],
      ],
    ];

    // @TODO Title to show (include token support).
    // Submission guidelines.
    // Can make all this optional, e.g. it could be done via block

    // Submission settings.
    $form['form_pack']['submission_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Submission settings'),
      '#states' => [
        'visible' => [
          '.form_pack_enabled' => ['checked' => TRUE],
        ],
      ],
    ];

    // @TODO Action:
    // - go somewhere
    // - show message / page / rendered submission
    //
    $form['actions']['submit']['#submit'][] = 'Drupal\form_pack\Service\FormPackGeneralService::entityBundleEditFormSubmit';
  }

  /**
   * Handle the submission of an entity bundle edit form.
   */
  public static function entityBundleEditFormSubmit(array &$form, FormStateInterface $form_state) {
    $entity = $form_state->getFormObject()->getEntity();
    $values = $form_state->getValue('form_pack');
    $entity->setThirdPartySetting('form_pack', 'form_pack', $values)->save();;
  }

}
